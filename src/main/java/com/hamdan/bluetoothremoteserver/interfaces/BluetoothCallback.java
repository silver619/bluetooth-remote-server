package com.hamdan.bluetoothremoteserver.interfaces;

/**
 * Callback interface for listening and connected threads to communicate 
 * back to main thread
 */
public interface BluetoothCallback {
	void notifyStatus(int state);
	void notifyDeviceName(String device);
}