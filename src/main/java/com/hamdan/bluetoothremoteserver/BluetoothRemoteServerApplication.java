package com.hamdan.bluetoothremoteserver;

import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;
import org.pmw.tinylog.writers.FileWriter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.hamdan.bluetoothremoteserver.bluetooth.ConnectionListener;
import com.hamdan.bluetoothremoteserver.interfaces.BluetoothCallback;
import com.hamdan.bluetoothremoteserver.ui.MainSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Main Class that creates a Swing interface and starts a Listener Thread which waits
 * for connection attempts from other devices. Class implements a BluetoothCallback to
 * receive status and device information from threads.
 */
@SpringBootApplication
public class BluetoothRemoteServerApplication implements CommandLineRunner, BluetoothCallback {
    private String deviceName = "";
    private MainSwing mainSwing;
    
    private ConnectionListener connectionListener;
	
	public static void main(String[] args) {
    	new SpringApplicationBuilder(BluetoothRemoteServerApplication.class).headless(false).run(args);
	
    	Configurator.defaultConfig()
	 	   .writer(new FileWriter("serverlog.txt", false, true))
	 	   .level(Level.INFO)
	 	   .formatPattern("{date: dd-MM-YYYY HH:mm:ss} - {level}: {message}")
	 	   .activate();
    	
    	Logger.info("Server Started!");
	}    

	@Override
	public void run(String... args) throws Exception {
		
		// setup a Swing interface
		mainSwing = new MainSwing("Bluetooth Remote Server", 300, 320);
		
		mainSwing.logsButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				logsButtonAction(e);				
			}
		});
        
        // Starts a Listener Thread to listen for a connection from a client
        connectionListener = new ConnectionListener(this);
        Thread listenerTrhead = new Thread(connectionListener);
        listenerTrhead.start();
	}

	@Override
	public void notifyStatus(int state) {
		switch (state) {
			case Constants.IDLE_STATE:				
				mainSwing.statusValueLabel.setText("Bluetooth Unavailable?");
				break;
			case Constants.LISTENING_STATE:
				mainSwing.statusValueLabel.setText("Waiting Connection");
				break;
			case Constants.CONNECTED_STATE:
				mainSwing.statusValueLabel.setText("Connected");		
				break;
			default:
				break;
		}
	}

	@Override
	public void notifyDeviceName(String device) {
		deviceName = device;
		mainSwing.deviceValueLabel.setText(deviceName);
	}
	
	// Opens the log file when button is pressed
	private void logsButtonAction(ActionEvent event) {
		try {
			java.awt.Desktop.getDesktop().edit(new File("serverlog.txt"));
		} catch (Exception e) {
			Logger.warn("Error opening log");
		    e.printStackTrace();
		} 
	}
}
