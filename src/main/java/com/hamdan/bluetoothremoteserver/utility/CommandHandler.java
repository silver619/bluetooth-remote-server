package com.hamdan.bluetoothremoteserver.utility;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;

import org.pmw.tinylog.Logger;

/**
 * Utility class for handling commands from a client device
 */
public class CommandHandler {
	Runtime runtime = Runtime.getRuntime();
	Robot robot;
	
	public CommandHandler() throws AWTException {
		robot = new Robot();
	}
	
	public void handleCommand(String command) throws IOException, AWTException {
		if(command.contains("x_x")) {
        	// handle keyboard commands
            String lastPart = command.split("x_x")[1];
            if(!lastPart.isEmpty()) {
            	for(int i=0; i<lastPart.length(); i++) {
            		char c = lastPart.toCharArray()[i];
                    AsciiKeyTyper keyTyper = new AsciiKeyTyper();
                    keyTyper.typeKey(c);
            	}                
            }
        } else if(command.contains(",")) {
        	// handle mouse movement commands
            float moveX = Float.parseFloat(command.split(",")[0]);
            float moveY = Float.parseFloat(command.split(",")[1]);
            Point point = MouseInfo.getPointerInfo().getLocation();
            robot.mouseMove((int) (point.x + moveX), (int) (point.y + moveY));
        } else {
        	// handle simple commands
            switch (command) {
                case "backspace":
                    robot.keyPress(KeyEvent.VK_BACK_SPACE);
                    robot.keyRelease(KeyEvent.VK_BACK_SPACE);
                    break;
                case "delete":
                    robot.keyPress(KeyEvent.VK_DELETE);
                    robot.keyRelease(KeyEvent.VK_DELETE);
                    break;
                case "back":
                    robot.keyPress(KeyEvent.VK_LEFT);
                    robot.keyRelease(KeyEvent.VK_LEFT);
                    break;
                case "stop":
                    robot.keyPress(KeyEvent.VK_B);
                    robot.keyRelease(KeyEvent.VK_B);
                    break;
                case "forward":
                    robot.keyPress(KeyEvent.VK_RIGHT);
                    robot.keyRelease(KeyEvent.VK_RIGHT);
                    break;
                case "restart_slide":
                    robot.keyPress(KeyEvent.VK_F5);
                    robot.keyRelease(KeyEvent.VK_F5);
                    break;
                case "quit":
                    robot.keyPress(KeyEvent.VK_ESCAPE);
                    robot.keyRelease(KeyEvent.VK_ESCAPE);
                    break;
                case "l_click_single":
                    robot.mousePress(MouseEvent.BUTTON1_DOWN_MASK);
                    robot.mouseRelease(MouseEvent.BUTTON1_DOWN_MASK);
                    break;
                case "l_click_down":
                    robot.mousePress(MouseEvent.BUTTON1_DOWN_MASK);
                    break;
                case "l_click_up":
                    robot.mouseRelease(MouseEvent.BUTTON1_DOWN_MASK);
                    break;
                case "r_click_down":
                    robot.mousePress(MouseEvent.BUTTON3_DOWN_MASK);
                    break;
                case "r_click_up":
                    robot.mouseRelease(MouseEvent.BUTTON3_DOWN_MASK);
                    break;
                case "power":
                	Logger.info("Remote use shut down system");
                    runtime.exec("shutdown -s -t 0");
                    System.exit(0);
                    break;
                case "restart":
                	Logger.info("Remote use restarted system");
                    runtime.exec("shutdown -r -t 0");
                    System.exit(0);
                    break;
                case "hibernate":
                	Logger.info("Remote use hibernated system");
                    runtime.exec("shutdown -h");
                    break;
                case "logoff":
                	Logger.info("Remote use logged out of system");
                    runtime.exec("shutdown -l");
                    break;
                default:
                    break;
            }
        }
	}
}
