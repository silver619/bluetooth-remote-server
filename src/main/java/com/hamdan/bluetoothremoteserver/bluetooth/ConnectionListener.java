package com.hamdan.bluetoothremoteserver.bluetooth;

import java.io.IOException;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

import org.pmw.tinylog.Logger;

import com.hamdan.bluetoothremoteserver.Constants;
import com.hamdan.bluetoothremoteserver.interfaces.BluetoothCallback;

/**
 * To be used in a listener Thread. Stays active while listening for a connection and
 * remains active to resume listening if a connection is dropped
 */
public class ConnectionListener implements Runnable {
	BluetoothCallback callback;
	LocalDevice localDevice;
    ConnectionHandler connectionHandler;
	boolean isListening = true;
	
	public ConnectionListener(BluetoothCallback callback) {
		this.callback = callback;
	}

	@Override
	public void run() {
		UUID uuid = new UUID(40130178);
        String url = "btspp://localhost:" + uuid.toString() + ";name=BluetoothRemote";
        
        StreamConnectionNotifier streamConnectionNotifier;
        StreamConnection streamConnection = null;
        try {
        	localDevice = LocalDevice.getLocalDevice();
            localDevice.setDiscoverable(DiscoveryAgent.GIAC);

            streamConnectionNotifier = (StreamConnectionNotifier) Connector.open(url);
        } catch(Exception e) {
        	Logger.warn("Failed to get local device!");
            e.printStackTrace();
            callback.notifyStatus(Constants.IDLE_STATE);
            return;
        }
        
        callback.notifyDeviceName(localDevice.getFriendlyName());
        callback.notifyStatus(Constants.LISTENING_STATE);
        
        // constantly loops so clients can always reconnect
        while(isListening) {
        	try {
				Thread.sleep(100);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	if(connectionHandler == null || !connectionHandler.isConnected) {
                try {                	
                	System.out.println("waiting for connection...");
    				streamConnection = streamConnectionNotifier.acceptAndOpen();
    			} catch (Exception e) {
    				Logger.warn(e.getMessage());
    				e.printStackTrace();
    			}
                connectionHandler = new ConnectionHandler(streamConnection, callback);
                Thread handleConnectionThread = new Thread(connectionHandler);
                handleConnectionThread.start();
                
                callback.notifyStatus(Constants.CONNECTED_STATE);
                System.out.println("Received OBEX connection ");
                
                /*
                 * Log remote device name and address. Getting the name slows the connection
                 * on the first attempt
                 */
                try {
        			RemoteDevice rd = RemoteDevice.getRemoteDevice(streamConnection);
        			Logger.info("Connection made to: " + rd.getFriendlyName(false) + " - " + rd.getBluetoothAddress());
        		} catch (IOException e) {
        			Logger.warn("Connect made, but could not get remote device name");
        			e.printStackTrace();
        		}
        	}        	
        }
        callback.notifyStatus(Constants.IDLE_STATE);
        System.out.println("Stopped Listening");
	}
}
