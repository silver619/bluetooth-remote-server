package com.hamdan.bluetoothremoteserver.bluetooth;

import java.awt.AWTException;
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.io.StreamConnection;
import org.pmw.tinylog.Logger;

import com.hamdan.bluetoothremoteserver.Constants;
import com.hamdan.bluetoothremoteserver.interfaces.BluetoothCallback;
import com.hamdan.bluetoothremoteserver.utility.CommandHandler;

/**
 * Handles a connection from a remote device and performs commands received from that device
 */
public class ConnectionHandler implements Runnable {
	StreamConnection streamConnection;
	BluetoothCallback btCallback;
	CommandHandler commandHandler;
	public boolean isConnected = true;
	
	public ConnectionHandler(StreamConnection streamConnection, BluetoothCallback btCallback) {
		this.streamConnection = streamConnection;
		this.btCallback = btCallback;
	}

	@Override
	public void run() {		
        byte[] buffer = new byte[1024];
        int numBytes;        
        InputStream inputStream;
        
        try {
			commandHandler = new CommandHandler();
		} catch (AWTException e1) {
			e1.printStackTrace();
			isConnected = false;
		}
        
		try {
			inputStream = streamConnection.openInputStream();
			System.out.println("Waiting for input...");
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

        while(isConnected) {
            try {
                numBytes = inputStream.read(buffer);
                
                // close connection if end of stream
                if (numBytes == -1) {
                    isConnected = false;
                    System.out.println("Closing Connection");
                } else {
                	// convert bytes to a string command and then performs the command
                    String command = new String(buffer, 0, numBytes);
                    System.out.println("Input Received: " + command);
                    
                    commandHandler.handleCommand(command);
                    
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Logger.info("Connection with remote user was lost");
        btCallback.notifyStatus(Constants.LISTENING_STATE);
	}
}
