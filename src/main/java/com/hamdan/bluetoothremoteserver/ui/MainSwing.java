package com.hamdan.bluetoothremoteserver.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Swing interface for the application
 */
public class MainSwing extends JFrame {

	private static final long serialVersionUID = 1L;
	
	public JLabel statusValueLabel;
	public JLabel deviceValueLabel;
	public JButton logsButton;
	
	public MainSwing(String title, int width, int height) {
		JFrame frame = new JFrame(title);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setSize(width,height);
	    
	    JPanel contentPane = new JPanel();
		contentPane.setBackground(new Color(218, 191, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		frame.setContentPane(contentPane);
	    
	    JPanel titlePanel = new JPanel();
	    titlePanel.setBackground(new Color(218, 191, 255));
	    JLabel titleLabel = new JLabel("Bluetooth Remote");
	    titleLabel.setFont(new Font("Serif", Font.PLAIN, 34));
	    titlePanel.add(titleLabel);
	    
	    JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
	    centerPanel.setBackground(new Color(218, 191, 255));
	    centerPanel.setBorder(new EmptyBorder(30, 10, 10, 10));
	    
	    JLabel stautsLabel = new JLabel("Status:");
	    centerPanel.add(stautsLabel);
	    
	    statusValueLabel = new JLabel("Idle");
	    centerPanel.add(statusValueLabel);
	    
	    JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 10));
	    bottomPanel.setBackground(new Color(218, 191, 255));
	    bottomPanel.setBorder(new EmptyBorder(10, 10, 20, 10));
	    
	    JLabel deviceIdLabel = new JLabel("Device ID:");
	    bottomPanel.add(deviceIdLabel);
	    
	    deviceValueLabel = new JLabel("");
	    bottomPanel.add(deviceValueLabel);
	    
	    logsButton = new JButton("View Log");
	    
	    bottomPanel.add(logsButton);        
	    frame.getContentPane().add(BorderLayout.NORTH, titlePanel);
	    frame.getContentPane().add(BorderLayout.CENTER, centerPanel);
	    frame.getContentPane().add(BorderLayout.SOUTH, bottomPanel);
	    frame.setVisible(true);
	}

}
