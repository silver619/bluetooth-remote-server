package com.hamdan.bluetoothremoteserver;

public interface Constants {
	// States
	int IDLE_STATE = 0;
	int LISTENING_STATE = 1;
	int CONNECTED_STATE = 2;
}
